#include "class_descriptor.hpp"

#include "link_resolver.hpp"
#include "util.hpp"

#include <iostream>

#include <boost/spirit/home/x3.hpp>

#include <mstch/mstch.hpp>

namespace wtfm {
	namespace detail {
		const boost::spirit::x3::rule<struct class_comment_class, wtfm::class_descriptor::comment_t> class_comment =
		        "class_comment";
		auto class_comment_def = rules::paragraph >> rules::paragraph;
		BOOST_SPIRIT_DEFINE(class_comment)
	}

	class_descriptor::class_descriptor(const config& config, const cppast::cpp_class& entity)
	    : _config(&config), _entity(&entity), _constructor(config) {
		entity.comment().map([this](auto&& comment_string) {
			boost::spirit::x3::phrase_parse(comment_string.cbegin() + 1,
			                                comment_string.cend() - 1,
			                                detail::class_comment,
			                                boost::spirit::x3::space,
			                                _comment);
		});
	}

	auto class_descriptor::generate() const -> void {
		auto full_qualified_name = wtfm::full_qualified_name(*_entity);
		auto filename            = full_qualified_name + ".html";
		auto path                = _config->output / filename;
		auto parent_name_length  = full_qualified_name.rfind("::");
		auto parent_name =
		        parent_name_length != std::string::npos ? full_qualified_name.substr(0, parent_name_length + 2) : "";
		auto entity_name = parent_name_length != std::string::npos ? full_qualified_name.substr(parent_name_length + 2)
		                                                           : full_qualified_name;

		mstch::array descriptions;

		if (!_enums.empty()) {
			descriptions.emplace_back(
			        mstch::map{{"description_heading", std::string{"Nested enums"}}, {"description", mstch::array{}}});
			auto& description = boost::get<mstch::array>(boost::get<mstch::map>(descriptions.back())["description"]);
			for (auto&& [name, enum_descriptor] : _enums) {
				enum_descriptor.generate();
				description.emplace_back(
				        mstch::map{{"name", name},
				                   {"url", link_resolver::resolve(*_config, enum_descriptor.full_qualified_name())},
				                   {"brief", enum_descriptor.comment().brief}});
			}
		}

		if (!_classes.empty()) {
			descriptions.emplace_back(
			        mstch::map{{"description_heading", std::string{"Nested classes"}}, {"description", mstch::array{}}});
			auto& description = boost::get<mstch::array>(boost::get<mstch::map>(descriptions.back())["description"]);
			for (auto&& [name, class_descriptor] : _classes) {
				class_descriptor.generate();
				description.emplace_back(
				        mstch::map{{"name", name},
				                   {"url", link_resolver::resolve(*_config, class_descriptor.full_qualified_name())},
				                   {"brief", class_descriptor.comment().brief}});
			}
		}

		if (!_friends.empty()) {
			descriptions.emplace_back(mstch::map{
			        {"description_heading", std::string{"Friends"}}, {"code", true}, {"description", mstch::array{}}});
			auto& description = boost::get<mstch::array>(boost::get<mstch::map>(descriptions.back())["description"]);
			for (auto&& [name, friend_descriptor] : _friends) {
				description.emplace_back(
				        mstch::map{{"name", friend_descriptor.name()},
				                   {"url", link_resolver::resolve(*_config, friend_descriptor.full_qualified_name())}});
			}
		}

		if (!_member_types.empty()) {
			descriptions.emplace_back(mstch::map{
			        {"description_heading", std::string{"Member types"}},
			        {"code", true},
			        {"sub_heading", mstch::map{{"first", std::string("Member type")}, {"second", std::string("Definition")}}},
			        {"description", mstch::array{}}});
			auto& description = boost::get<mstch::array>(boost::get<mstch::map>(descriptions.back())["description"]);
			for (auto&& [name, type_alias_descriptor] : _member_types) {
				description.emplace_back(mstch::map{{"name", name}, {"brief", type_alias_descriptor.definition()}});
			}
		}

		if (!_member_functions.empty() || !_constructor.empty() || _destructor) {
			descriptions.emplace_back(
			        mstch::map{{"description_heading", std::string{"Member functions"}}, {"description", mstch::array{}}});
			auto& description = boost::get<mstch::array>(boost::get<mstch::map>(descriptions.back())["description"]);

			if (!_constructor.empty()) {
				_constructor.generate();
				description.emplace_back(
				        mstch::map{{"name", std::string("(constructor)")},
				                   {"url", link_resolver::resolve(*_config, _constructor.full_qualified_name())},
				                   {"brief", _constructor.brief()}});
			}

			if (_destructor) {
				auto& destructor = *_destructor;
				_destructor->generate();
				description.emplace_back(mstch::map{{"name", std::string("(destructor)")},
				                                    {"url", link_resolver::resolve(*_config, destructor.full_qualified_name())},
				                                    {"brief", destructor.comment().brief}});
			}

			for (auto&& [name, member_function_descriptor] : _member_functions) {
				member_function_descriptor.generate();
				description.emplace_back(
				        mstch::map{{"name", name},
				                   {"url", link_resolver::resolve(*_config, member_function_descriptor.full_qualified_name())},
				                   {"brief", member_function_descriptor.brief()}});
			}
		}

		if (!_conversion_operators.empty()) {
			descriptions.emplace_back(mstch::map{{"description_heading", std::string{"Conversion operators"}},
			                                     {"description", mstch::array{}}});
			auto& description = boost::get<mstch::array>(boost::get<mstch::map>(descriptions.back())["description"]);
			for (auto&& [name, conversion_operator_descriptor] : _conversion_operators) {
				conversion_operator_descriptor.generate();
				description.emplace_back(mstch::map{
				        {"name", name},
				        {"url", link_resolver::resolve(*_config, conversion_operator_descriptor.full_qualified_name())},
				        {"brief", conversion_operator_descriptor.comment().brief}});
			}
		}

		if (!_member_variables.empty()) {
			descriptions.emplace_back(
			        mstch::map{{"description_heading", std::string{"Member variables"}}, {"description", mstch::array{}}});
			auto& description = boost::get<mstch::array>(boost::get<mstch::map>(descriptions.back())["description"]);
			for (auto&& [name, member_variable_descriptor] : _member_variables) {
				member_variable_descriptor.generate();
				description.emplace_back(
				        mstch::map{{"name", name},
				                   {"url", link_resolver::resolve(*_config, member_variable_descriptor.full_qualified_name())},
				                   {"brief", member_variable_descriptor.comment().brief}});
			}
		}

		//		for (auto&& description_entry : descriptions) {
		//			auto& description = boost::get<mstch::array>(boost::get<mstch::map>(description_entry)["description"]);
		//			std::sort(description.begin(), description.end(), [](auto& a, auto& b) {
		//				auto& entry_a = boost::get<std::string>(boost::get<mstch::map>(a)["name"]);
		//				auto& entry_b = boost::get<std::string>(boost::get<mstch::map>(b)["name"]);
		//				return entry_a <= entry_b;
		//			});
		//		}

		mstch::array nested_classes;

		std::ofstream os(path.native(), std::ofstream::trunc);
		std::ifstream is("class.mustache");
		std::string   page((std::istreambuf_iterator<char>(is)), std::istreambuf_iterator<char>());
		mstch::map    context{{"project_name", _config->name},
                       {"scope", full_qualified_name},
                       {"parent_scope", parent_name},
                       {"name", entity_name},
                       {"defining_header", defining_header()},
                       {"class_kind", class_kind()},
                       {"brief", _comment.brief},
                       {"detailed", _comment.detailed},
                       {"descriptions", descriptions}};

		os << mstch::render(page, context) << std::endl;
	}
}
