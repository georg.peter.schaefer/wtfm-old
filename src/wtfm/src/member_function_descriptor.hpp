#pragma once

#include "config.hpp"
#include "util.hpp"

#include <string>
#include <vector>

#include <boost/fusion/include/adapt_struct.hpp>

#include <cppast/cpp_member_function.hpp>

#include <mstch/mstch.hpp>

namespace wtfm {
	class member_function_descriptor {
	public:
		class overload {
			friend class member_function_descriptor;

		public:
			struct comment_t {
				std::string              brief;
				std::string              detailed;
				std::vector<parameter_t> parameters;
				std::string              returns;
			};

			overload(const cppast::cpp_member_function& entity);

			auto full_qualified_name() const {
				return wtfm::full_qualified_name(*_entity);
			}

			auto return_type() const -> std::string {
				return cppast::to_string(_entity->return_type());
			}

			auto parameters() const -> mstch::array;

			auto body_type() const -> std::string {
				switch (_entity->body_kind()) {
					case cppast::cpp_function_body_kind::cpp_function_declaration:
					case cppast::cpp_function_body_kind::cpp_function_definition:
						return ";";
					case cppast::cpp_function_body_kind ::cpp_function_defaulted:
						return " = default;";
					case cppast::cpp_function_body_kind::cpp_function_deleted:
						return " = delete;";
					default:
						throw std::runtime_error("Unhandled function body type.");
				}
			}

			auto constexpr_specifier() const -> std::string {
				if (_entity->is_constexpr()) {
					return "constexpr ";
				}
				return "";
			}

			auto virtual_specifier() const -> std::string {
				auto virtual_info = _entity->virtual_info();
				if (cppast::is_virtual(virtual_info) || cppast::is_pure(virtual_info)) {
					return "virtual ";
				} else {
					return "";
				}
			}

			auto cv_qualifier() const -> std::string {
				switch (_entity->cv_qualifier()) {
					case cppast::cpp_cv::cpp_cv_none:
						return "";
					case cppast::cpp_cv::cpp_cv_const:
						return " const";
					case cppast::cpp_cv::cpp_cv_volatile:
						return " volatile";
					case cppast::cpp_cv::cpp_cv_const_volatile:
						return " const volatile";
					default:
						throw std::runtime_error("Unhandled cv qualifier.");
				}
			}

			auto noexcept_specifier() const -> std::string {
				return _entity->noexcept_condition().has_value() ? " noexcept" : std::string();
			}

			auto virtual_info() const -> std::string {
				auto virtual_info = _entity->virtual_info();
				if (cppast::is_pure(virtual_info)) {
					return " = 0";
				} else if (cppast::is_overriding(virtual_info)) {
					return " override";
				} else if (cppast::is_final(virtual_info)) {
					return " final";
				} else {
					return "";
				}
			}

			auto& comment() const {
				return _comment;
			}

		private:
			const cppast::cpp_member_function* _entity;
			comment_t                          _comment;
		};

		member_function_descriptor(const config& config);

		auto add(const cppast::cpp_member_function& entity) {
			_overloads.emplace_back(entity);
		}

		auto generate() const -> void;

		auto full_qualified_name() const {
			return _overloads.front().full_qualified_name();
		}

		auto& brief() const {
			return _overloads.front().comment().brief;
		}

		auto parameters() const -> mstch::array;

		auto begin() {
			return _overloads.begin();
		}

		auto begin() const {
			return _overloads.begin();
		}

		auto end() {
			return _overloads.end();
		}

		auto end() const {
			return _overloads.end();
		}

	private:
		const config*         _config;
		std::vector<overload> _overloads;
	};
}

BOOST_FUSION_ADAPT_STRUCT(wtfm::member_function_descriptor::overload::comment_t, brief, detailed, parameters, returns)
