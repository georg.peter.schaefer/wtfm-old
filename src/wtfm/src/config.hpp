#pragma once

#include <string>
#include <vector>

#include <boost/filesystem.hpp>
#include <boost/optional.hpp>

namespace wtfm {
	struct config {
		using path_list    = std::vector<boost::filesystem::path>;
		using feature_list = std::vector<std::string>;

		std::string                              name;
		path_list                                input;
		boost::filesystem::path                  output;
		path_list                                include;
		boost::optional<path_list>               exclude;
		boost::optional<boost::filesystem::path> compile_commands;
		boost::optional<feature_list>            features;
	};
}
