#pragma once

#include "config.hpp"
#include "util.hpp"

#include <string>
#include <vector>

#include <boost/fusion/include/adapt_struct.hpp>

#include <cppast/cpp_function.hpp>

#include <mstch/mstch.hpp>

namespace wtfm {
	class function_descriptor {
	public:
		class overload {
			friend class function_descriptor;

		public:
			struct comment_t {
				std::string              brief;
				std::string              detailed;
				std::vector<parameter_t> parameters;
				std::string              returns;
			};

			overload(const cppast::cpp_function& entity);

			auto full_qualified_name() const {
				return wtfm::full_qualified_name(*_entity);
			}

			auto return_type() const -> std::string {
				return cppast::to_string(_entity->return_type());
			}

			auto parameters() const -> mstch::array;

			auto body_type() const -> std::string {
				switch (_entity->body_kind()) {
					case cppast::cpp_function_body_kind::cpp_function_declaration:
					case cppast::cpp_function_body_kind::cpp_function_definition:
						return ";";
					case cppast::cpp_function_body_kind ::cpp_function_defaulted:
						return " = default;";
					case cppast::cpp_function_body_kind::cpp_function_deleted:
						return " = delete;";
					default:
						throw std::runtime_error("Unhandled function body type.");
				}
			}

			auto noexcept_specifier() const -> std::string {
				return _entity->noexcept_condition().has_value() ? " noexcept" : std::string();
			}

			auto& comment() const {
				return _comment;
			}

		private:
			const cppast::cpp_function* _entity;
			comment_t                   _comment;
		};

		function_descriptor(const config& config);

		auto add(const cppast::cpp_function& entity) {
			_overloads.emplace_back(entity);
		}

		auto generate() const -> void;

		auto full_qualified_name() const {
			return _overloads.front().full_qualified_name();
		}

		auto defining_header() const {
			return wtfm::defining_header(*_config, *_overloads.back()._entity);
		}

		auto& brief() const {
			return _overloads.front().comment().brief;
		}

		auto parameters() const -> mstch::array;

		auto begin() {
			return _overloads.begin();
		}

		auto begin() const {
			return _overloads.begin();
		}

		auto end() {
			return _overloads.end();
		}

		auto end() const {
			return _overloads.end();
		}

	private:
		const config*         _config;
		std::vector<overload> _overloads;
	};
}

BOOST_FUSION_ADAPT_STRUCT(wtfm::function_descriptor::overload::comment_t, brief, detailed, parameters, returns)
