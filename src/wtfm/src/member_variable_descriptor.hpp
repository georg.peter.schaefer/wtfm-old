#pragma once

#include "config.hpp"
#include "util.hpp"

#include <string>

#include <boost/fusion/include/adapt_struct.hpp>

#include <cppast/cpp_member_variable.hpp>

namespace wtfm {
	class member_variable_descriptor {
	public:
		struct comment_t {
			std::string brief;
			std::string detailed;
		};

		member_variable_descriptor(const config& config, const cppast::cpp_member_variable& entity);

		auto generate() const -> void;

		auto full_qualified_name() const {
			return wtfm::full_qualified_name(*_entity);
		}

		auto defining_header() const {
			return wtfm::defining_header(*_config, *_entity);
		}

		auto definition() const -> std::string;

		auto& comment() const {
			return _comment;
		}

	private:
		const config*                      _config;
		const cppast::cpp_member_variable* _entity;
		comment_t                          _comment;
	};
}

BOOST_FUSION_ADAPT_STRUCT(wtfm::member_variable_descriptor::comment_t, brief, detailed)
