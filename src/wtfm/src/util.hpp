#pragma once

#include "config.hpp"

#include <string>
#include <vector>

#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/spirit/home/x3.hpp>
#include <boost/spirit/home/x3/support/ast/position_tagged.hpp>

#include <cppast/cpp_entity.hpp>
#include <cppast/cpp_expression.hpp>

namespace wtfm {
	struct parameter_t : boost::spirit::x3::position_tagged {
		parameter_t(const std::string& name = "", const std::string& description = "")
		    : name(name), description(description) {}
		std::string name;
		std::string description;
	};
}
BOOST_FUSION_ADAPT_STRUCT(wtfm::parameter_t, name, description)

namespace wtfm {
	constexpr auto indent = "  ";

	auto hierarchy(const cppast::cpp_entity& entity) -> std::vector<const cppast::cpp_entity*>;
	auto full_qualified_name(const cppast::cpp_entity& entity) -> std::string;
	auto defining_header(const config& config, const cppast::cpp_entity& entity) -> std::string;
	auto expression(const cppast::cpp_expression& expression) -> std::string;

	namespace rules {
		const inline boost::spirit::x3::rule<struct paragraph_class, std::string> paragraph = "paragraph";
		auto const                                                                paragraph_def =
		        boost::spirit::x3::lexeme[+(boost::spirit::x3::char_ - (boost::spirit::x3::eol >> +boost::spirit::x3::eol))
		                                  >> *boost::spirit::x3::eol];
		BOOST_SPIRIT_DEFINE(paragraph)

		const inline boost::spirit::x3::rule<struct parameter_identifier_class, std::string> parameter_identifier =
		        "parameter_identifier";
		auto const parameter_identifier_def = boost::spirit::x3::lexeme[+boost::spirit::x3::char_("a-zA-Z0-9_")];
		BOOST_SPIRIT_DEFINE(parameter_identifier)

		const inline boost::spirit::x3::rule<struct parameter_description_class, std::string> parameter_description =
		        "parameter_description";
		auto const parameter_description_def =
		        boost::spirit::x3::lexeme[+(boost::spirit::x3::char_ - boost::spirit::x3::lit("@"))];
		BOOST_SPIRIT_DEFINE(parameter_description)

		const inline boost::spirit::x3::rule<struct parameter_class, parameter_t> parameter = "parameter";
		auto const parameter_def = *boost::spirit::x3::eol >> boost::spirit::x3::lit("@param") >> parameter_identifier
		                           >> parameter_description;
		BOOST_SPIRIT_DEFINE(parameter)

		const inline boost::spirit::x3::rule<struct returns_class, std::string> returns = "returns";
		auto const                                                              returns_def =
		        boost::spirit::x3::lit("@returns")
		        >> parameter_description;    //*boost::spirit::x3::eol >> boost::spirit::x3::lit("@returns")
		                                     //>> boost::spirit::x3::lexeme[+(boost::spirit::x3::char_)];
		BOOST_SPIRIT_DEFINE(returns)
	}
}
