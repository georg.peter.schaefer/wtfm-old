#include "function_descriptor.hpp"

#include "html.hpp"
#include "link_resolver.hpp"
#include "util.hpp"

#include <algorithm>
#include <iostream>

#include <boost/algorithm/string/replace.hpp>
#include <boost/spirit/home/x3.hpp>

namespace wtfm {
	namespace detail {
		const boost::spirit::x3::rule<class function_comment_class, wtfm::function_descriptor::overload::comment_t>
		           function_comment     = "function_comment";
		const auto function_comment_def = rules::paragraph >> rules::paragraph >> *rules::parameter >> rules::returns;
		BOOST_SPIRIT_DEFINE(function_comment)
	}

	function_descriptor::overload::overload(const cppast::cpp_function& entity) : _entity(&entity) {
		entity.comment().map([this](auto&& comment_string) {
			boost::spirit::x3::phrase_parse(comment_string.begin() + 1,
			                                comment_string.end() - 1,
			                                detail::function_comment,
			                                boost::spirit::x3::space,
			                                _comment);
			boost::replace_all(_comment.brief, "\n", " ");
			boost::replace_all(_comment.detailed, "\n", " ");
			for (auto&& parameter : _comment.parameters) {
				boost::replace_all(parameter.description, "\n", " ");
			}
		});
	}

	auto function_descriptor::overload::parameters() const -> mstch::array {
		mstch::array parameters;
		auto         index = 0;
		for (auto&& parameter : _entity->parameters()) {
			++index;
			mstch::map param;
			param["type"] = cppast::to_string(parameter.type());
			param["name"] = parameter.name();
			if (auto description_it = std::find_if(
			            _comment.parameters.begin(),
			            _comment.parameters.end(),
			            [&parameter](auto&& parameter_comment) { return parameter_comment.name == parameter.name(); });
			    description_it != _comment.parameters.end()) {
				param["description"] = description_it->description;
			}
			if (index == std::distance(_entity->parameters().begin(), _entity->parameters().end())) {
				param["last"] = true;
			}
			parameters.emplace_back(param);
		}
		return parameters;
	}

	function_descriptor::function_descriptor(const config& config) : _config(&config) {}

	auto function_descriptor::generate() const -> void {
		auto full_qualified_name = _overloads.front().full_qualified_name();
		auto filename            = full_qualified_name + ".html";
		auto path                = _config->output / filename;
		auto parent_name_length  = full_qualified_name.rfind("::");
		auto parent_name =
		        parent_name_length != std::string::npos ? full_qualified_name.substr(0, parent_name_length + 2) : "";
		auto entity_name = parent_name_length != std::string::npos ? full_qualified_name.substr(parent_name_length + 2)
		                                                           : full_qualified_name;

		mstch::array overloads;
		mstch::array overload_descriptions;
		struct return_description_t {
			std::string prefix;
			std::string description;
		};
		std::vector<return_description_t> returns;
		auto                              overload_number = 1;
		for (auto&& overload : _overloads) {
			mstch::map definition;
			definition["return_type"] = overload.return_type();
			definition["name"]        = overload._entity->name();
			definition["parameters"]  = overload.parameters();
			definition["noexcept"]    = overload.noexcept_specifier();
			definition["body_type"]   = overload.body_type();
			if (_overloads.size() > 1) {
				definition["overload"] = "(" + std::to_string(overload_number) + ")";
			}

			overloads.emplace_back(definition);
			overload_descriptions.emplace_back(
			        mstch::map{{"number", overload_number}, {"description", overload.comment().detailed}});

			auto& returns_description = overload.comment().returns;
			if (!returns_description.empty()) {
				if (auto returns_it = std::find_if(returns.begin(),
				                                   returns.end(),
				                                   [&returns_description](auto&& description) {
					                                   return returns_description == description.description;
				                                   });
				    returns_it != returns.end()) {
					returns_it->prefix += ", " + std::to_string(overload_number);
				} else {
					returns.emplace_back(return_description_t{std::to_string(overload_number), returns_description});
				}
			}

			++overload_number;
		}
		mstch::array return_descriptions;
		std::transform(returns.begin(), returns.end(), std::back_inserter(return_descriptions), [](auto&& value) {
			return mstch::map{{"number", value.prefix}, {"description", value.description}};
		});

		std::ofstream os(path.native(), std::ofstream::trunc);
		std::ifstream is("function.mustache");
		std::string   page((std::istreambuf_iterator<char>(is)), std::istreambuf_iterator<char>());
		mstch::map    context{{"project_name", _config->name},
                       {"scope", full_qualified_name},
                       {"parent_scope", parent_name},
                       {"name", entity_name},
                       {"defining_header", defining_header()},
                       {"overloads", overloads},
                       {"brief", brief()},
                       {"overload_descriptions", overload_descriptions},
                       {"parameters", parameters()},
                       {"returns", return_descriptions}};

		os << mstch::render(page, context) << std::endl;
	}

	auto function_descriptor::parameters() const -> mstch::array {
		std::vector<mstch::array> length_ordered_parameter_list;
		for (auto&& overload : _overloads) {
			length_ordered_parameter_list.emplace_back(overload.parameters());
		}
		std::sort(length_ordered_parameter_list.begin(), length_ordered_parameter_list.end(), [](auto&& a, auto&& b) {
			return a.size() < b.size();
		});
		mstch::array combined_parameters;
		for (auto&& parameter_list : length_ordered_parameter_list) {
			for (auto&& parameter : parameter_list) {
				if (auto parameter_it =
				            std::find_if(combined_parameters.begin(),
				                         combined_parameters.end(),
				                         [&parameter](auto&& elem) {
					                         auto inserted_param_name =
					                                 boost::get<std::string>(boost::get<mstch::map>(elem)["name"]);
					                         auto param_name = boost::get<std::string>(boost::get<mstch::map>(parameter)["name"]);
					                         return inserted_param_name == param_name;
				                         });
				    parameter_it == combined_parameters.end()) {
					combined_parameters.emplace_back(parameter);
				}
			}
		}
		return combined_parameters;
	}
}
