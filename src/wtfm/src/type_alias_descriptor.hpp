#pragma once

#include "config.hpp"

#include <cppast/cpp_type_alias.hpp>

namespace wtfm {
	class type_alias_descriptor {
	public:
		type_alias_descriptor(const config& config, const cppast::cpp_type_alias& entity)
		    : _config(&config), _entity(&entity) {}

		auto name() const noexcept {
			return _entity->name();
		}

		auto definition() const noexcept {
			return cppast::to_string(_entity->underlying_type());
		}

	private:
		[[maybe_unused]] const config* _config;
		const cppast::cpp_type_alias*  _entity;
	};
}
