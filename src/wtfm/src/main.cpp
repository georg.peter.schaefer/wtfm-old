#include "config.hpp"
#include "documentation.hpp"
#include "info.hpp"

#include <iostream>

#include <boost/program_options.hpp>

int main(int argc, char** argv) {
	// clang-format off
	boost::program_options::options_description generic_options("generic options");
	generic_options.add_options()
	        ("version,v", "print version string")
	        ("help", "produce help message")
	        ("config-file,c", boost::program_options::value<boost::filesystem::path>(), "path to configuration file");

	boost::program_options::options_description config_options("configuration");
	config_options.add_options()
	        ("name", boost::program_options::value<std::string>()->required(), "project name")
	        ("input", boost::program_options::value<wtfm::config::path_list>()->multitoken()->required(), "input directories")
	        ("output", boost::program_options::value<boost::filesystem::path>()->required(), "output directory")
	        ("include-directories,I", boost::program_options::value<wtfm::config::path_list>()->multitoken()->required(), "include directories")
	        ("exclude-directories,E", boost::program_options::value<wtfm::config::path_list>()->multitoken(), "exclude directories")
	        ("compile-commands", boost::program_options::value<boost::filesystem::path>(), "path to compile_commands.json")
	        ("feature,f", boost::program_options::value<wtfm::config::feature_list>(), "enable -fXXX feature");
	// clang-format on

	boost::program_options::options_description cmdline_options;
	cmdline_options.add(generic_options).add(config_options);

	boost::program_options::options_description config_file_options;
	config_file_options.add(config_options);

	boost::program_options::variables_map vm;
	boost::program_options::store(boost::program_options::parse_command_line(argc, argv, cmdline_options), vm);

	if (vm.empty() || vm.count("help")) {
		std::cout << cmdline_options << std::endl;
		return 1;
	}

	if (vm.count("version")) {
		std::cout << "wtfm version " << wtfm::version::major << "." << wtfm::version::minor << "." << wtfm::version::patch
		          << std::endl;
		return 1;
	}

	boost::program_options::notify(vm);

	if (vm.count("config-file")) {
		boost::program_options::store(
		        boost::program_options::parse_config_file(vm["config-file"].as<std::string>().c_str(), config_file_options),
		        vm);
		boost::program_options::notify(vm);
	}

	auto emplace_if = [](auto& vm, const auto& option, auto& optional) {
		if (vm.count(option)) {
			optional.emplace(vm[option].template as<typename std::decay_t<decltype(optional)>::value_type>());
		}
	};

	wtfm::config config;
	config.name    = vm["name"].as<std::string>();
	config.input   = vm["input"].as<wtfm::config::path_list>();
	config.output  = vm["output"].as<boost::filesystem::path>();
	config.include = vm["include-directories"].as<wtfm::config::path_list>();
	emplace_if(vm, "exclude-directories", config.exclude);
	emplace_if(vm, "compile-commands", config.compile_commands);
	emplace_if(vm, "feature", config.features);

	wtfm::documentation documentation(config);
	documentation.gather();
	documentation.generate();

	return 0;
}
