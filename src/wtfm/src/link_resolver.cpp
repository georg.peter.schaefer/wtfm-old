#include "link_resolver.hpp"

namespace wtfm {
	auto link_resolver::resolve(const config& config, const std::string& full_qualified_name, const std::string&)
	        -> std::string {
		auto& links = _links();
		if (auto link_it = links.find(full_qualified_name); link_it != links.end()) {
			return link_it->second;
		}

		auto filename = full_qualified_name + ".html";
		return (config.output / filename).native();
	}

	auto link_resolver::resolve(const config& config, const std::string& full_qualified_name) -> std::string {
		return resolve(config, full_qualified_name, "");
	}
}