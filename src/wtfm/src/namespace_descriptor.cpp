#include "namespace_descriptor.hpp"

#include "config.hpp"
#include "html.hpp"
#include "link_resolver.hpp"

#include <iostream>

#include <mstch/mstch.hpp>

#include <boost/algorithm/string/compare.hpp>
#include <boost/core/demangle.hpp>

namespace wtfm {
	namespace_descriptor::namespace_descriptor(const wtfm::config& config) : _config(&config), _entity(nullptr) {}

	namespace_descriptor::namespace_descriptor(const wtfm::config& config, const cppast::cpp_namespace& entity)
	    : _config(&config), _entity(&entity) {}

	auto namespace_descriptor::generate() const -> void {
		auto full_qualified_name = namespace_descriptor::full_qualified_name();
		auto filename            = full_qualified_name + ".html";
		auto path                = _config->output / filename;
		auto parent_name_length  = full_qualified_name.rfind("::");
		auto parent_name =
		        parent_name_length != std::string::npos ? full_qualified_name.substr(0, parent_name_length + 2) : "";
		auto entity_name = parent_name_length != std::string::npos ? full_qualified_name.substr(parent_name_length + 2)
		                                                           : full_qualified_name;
		mstch::array descriptions;

		if (!_namespaces.empty()) {
			descriptions.emplace_back(
			        mstch::map{{"description_heading", std::string{"Namespaces"}}, {"description", mstch::array{}}});
			auto& description = boost::get<mstch::array>(boost::get<mstch::map>(descriptions.back())["description"]);
			for (auto&& [name, namespace_descriptor] : _namespaces) {
				namespace_descriptor.generate();
				description.emplace_back(
				        mstch::map{{"name", name},
				                   {"url", link_resolver::resolve(*_config, namespace_descriptor.full_qualified_name())},
				                   {"brief", namespace_descriptor.comment().brief}});
			}
		}

		if (!_enums.empty()) {
			descriptions.emplace_back(
			        mstch::map{{"description_heading", std::string{"Enums"}}, {"description", mstch::array{}}});
			auto& description = boost::get<mstch::array>(boost::get<mstch::map>(descriptions.back())["description"]);
			for (auto&& [name, enum_descriptor] : _enums) {
				enum_descriptor.generate();
				description.emplace_back(
				        mstch::map{{"name", name},
				                   {"url", link_resolver::resolve(*_config, enum_descriptor.full_qualified_name())},
				                   {"brief", enum_descriptor.comment().brief}});
			}
		}

		if (!_classes.empty()) {
			descriptions.emplace_back(
			        mstch::map{{"description_heading", std::string{"Classes"}}, {"description", mstch::array{}}});
			auto& description = boost::get<mstch::array>(boost::get<mstch::map>(descriptions.back())["description"]);
			for (auto&& [name, class_descriptor] : _classes) {
				class_descriptor.generate();
				description.emplace_back(
				        mstch::map{{"name", name},
				                   {"url", link_resolver::resolve(*_config, class_descriptor.full_qualified_name())},
				                   {"brief", class_descriptor.comment().brief}});
			}
		}

		if (!_functions.empty()) {
			descriptions.emplace_back(
			        mstch::map{{"description_heading", std::string{"Functions"}}, {"description", mstch::array{}}});
			auto& description = boost::get<mstch::array>(boost::get<mstch::map>(descriptions.back())["description"]);
			for (auto&& [name, function_descriptor] : _functions) {
				function_descriptor.generate();
				description.emplace_back(
				        mstch::map{{"name", name},
				                   {"url", link_resolver::resolve(*_config, function_descriptor.full_qualified_name())},
				                   {"brief", function_descriptor.brief()}});
			}
		}

		if (!_variables.empty()) {
			descriptions.emplace_back(
			        mstch::map{{"description_heading", std::string{"Variables"}}, {"description", mstch::array{}}});
			auto& description = boost::get<mstch::array>(boost::get<mstch::map>(descriptions.back())["description"]);
			for (auto&& [name, variable_descriptor] : _variables) {
				variable_descriptor.generate();
				description.emplace_back(
				        mstch::map{{"name", name},
				                   {"url", link_resolver::resolve(*_config, variable_descriptor.full_qualified_name())},
				                   {"brief", variable_descriptor.comment().brief}});
			}
		}

		//		for (auto&& description_entry : descriptions) {
		//			auto& description = boost::get<mstch::array>(boost::get<mstch::map>(description_entry)["description"]);
		//			std::sort(description.begin(), description.end(), [](auto& a, auto& b) {
		//				auto& entry_a = boost::get<mstch::map>(a);
		//				auto& entry_b = boost::get<mstch::map>(b);
		//				return boost::get<std::string>(entry_a["name"]) <= boost::get<std::string>(entry_b["name"]);
		//			});
		//		}

		std::ofstream os(path.native(), std::ofstream::trunc);
		std::ifstream is("template.mustache");
		std::string   page((std::istreambuf_iterator<char>(is)), std::istreambuf_iterator<char>());
		mstch::map    context{{"project_name", _config->name},
                       {"scope", full_qualified_name},
                       {"parent_scope", parent_name},
                       {"name", entity_name},
                       {"definitions", mstch::array{mstch::map{{"definition", definition()}}}},
                       {"brief", _comment.brief},
                       {"detailed", _comment.detailed},
                       {"descriptions", descriptions}};

		os << mstch::render(page, context) << std::endl;
	}
}
