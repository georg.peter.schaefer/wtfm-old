#pragma once

#include "config.hpp"
#include "util.hpp"

#include <string>
#include <vector>

#include <boost/fusion/include/adapt_struct.hpp>

#include <cppast/cpp_member_function.hpp>

#include <mstch/mstch.hpp>

namespace wtfm {
	class constructor_descriptor {
	public:
		class overload {
			friend class constructor_descriptor;

		public:
			struct comment_t {
				std::string              brief;
				std::string              detailed;
				std::vector<parameter_t> parameters;
				std::string              returns;
			};

			overload(const cppast::cpp_constructor& entity);

			auto full_qualified_name() const {
				return wtfm::full_qualified_name(*_entity);
			}

			auto parameters() const -> mstch::array;

			auto body_type() const -> std::string {
				switch (_entity->body_kind()) {
					case cppast::cpp_function_body_kind::cpp_function_declaration:
					case cppast::cpp_function_body_kind::cpp_function_definition:
						return ";";
					case cppast::cpp_function_body_kind ::cpp_function_defaulted:
						return " = default;";
					case cppast::cpp_function_body_kind::cpp_function_deleted:
						return " = delete;";
					default:
						throw std::runtime_error("Unhandled function body type.");
				}
			}

			auto noexcept_specifier() const -> std::string {
				return _entity->noexcept_condition().has_value() ? " noexcept" : std::string();
			}

			auto& comment() const {
				return _comment;
			}

		private:
			const cppast::cpp_constructor* _entity;
			comment_t                      _comment;
		};

		constructor_descriptor(const config& config);

		auto add(const cppast::cpp_constructor& entity) {
			_overloads.emplace_back(entity);
		}

		auto generate() const -> void;

		auto full_qualified_name() const {
			return _overloads.front().full_qualified_name();
		}

		auto& brief() const {
			return _overloads.front().comment().brief;
		}

		auto parameters() const -> mstch::array;

		auto begin() {
			return _overloads.begin();
		}

		auto begin() const {
			return _overloads.begin();
		}

		auto end() {
			return _overloads.end();
		}

		auto end() const {
			return _overloads.end();
		}

		auto empty() const noexcept {
			return _overloads.empty();
		}

	private:
		const config*         _config;
		std::vector<overload> _overloads;
	};
}

BOOST_FUSION_ADAPT_STRUCT(wtfm::constructor_descriptor::overload::comment_t, brief, detailed, parameters, returns)
