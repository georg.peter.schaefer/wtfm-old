#pragma once

#include "config.hpp"
#include "constructor_descriptor.hpp"
#include "conversion_operator_descriptor.hpp"
#include "destructor_descriptor.hpp"
#include "enum_descriptor.hpp"
#include "friend_descriptor.hpp"
#include "member_function_descriptor.hpp"
#include "member_variable_descriptor.hpp"
#include "type_alias_descriptor.hpp"
#include "util.hpp"

#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/unordered_map.hpp>

#include <cppast/cpp_class.hpp>
#include <cppast/cpp_friend.hpp>

namespace wtfm {
	class class_descriptor {
	public:
		struct comment_t {
			std::string brief;
			std::string detailed;
		};

		class_descriptor(const config& config, const cppast::cpp_class& entity);

		auto add(const cppast::cpp_class& entity) {
			auto [iterator, emplaced] = _classes.emplace(entity.name(), class_descriptor(*_config, entity));
			return iterator;
		}

		auto add(const cppast::cpp_enum& entity) {
			auto [iterator, emplaced] = _enums.emplace(entity.name(), enum_descriptor(*_config, entity));
			return iterator;
		}

		auto add(const cppast::cpp_friend& entity) {
			_friends.emplace(entity.name(), friend_descriptor(*_config, entity));
		}

		auto add(const cppast::cpp_type_alias& entity) {
			auto [iterator, emplaced] = _member_types.emplace(entity.name(), type_alias_descriptor(*_config, entity));
			return iterator;
		}

		auto add(const cppast::cpp_member_variable& entity) {
			auto [iterator, emplaced] =
			        _member_variables.emplace(entity.name(), member_variable_descriptor(*_config, entity));
			return iterator;
		}

		auto add(const cppast::cpp_member_function& entity) {
			if (auto function_it = _member_functions.find(entity.name()); function_it != _member_functions.end()) {
				function_it->second.add(entity);
				return function_it;
			} else {
				auto [iterator, emplaced] = _member_functions.emplace(entity.name(), member_function_descriptor(*_config));
				iterator->second.add(entity);
				return iterator;
			}
		}

		auto add(const cppast::cpp_constructor& entity) {
			_constructor.add(entity);
		}

		auto destructor(const cppast::cpp_destructor& entity) {
			_destructor = desctructor_descriptor(*_config, entity);
		}

		auto add(const cppast::cpp_conversion_op& entity) {
			auto [iterator, emplaced] =
			        _conversion_operators.emplace(entity.name(), conversion_operator_descriptor(*_config, entity));
			return iterator;
		}

		auto generate() const -> void;

		auto full_qualified_name() const {
			return wtfm::full_qualified_name(*_entity);
		}

		auto defining_header() const {
			return wtfm::defining_header(*_config, *_entity);
		}

		auto class_kind() const -> std::string {
			return cppast::to_string(_entity->class_kind());
		}

		auto& comment() const {
			return _comment;
		}

	private:
		const config*                                                     _config;
		const cppast::cpp_class*                                          _entity;
		comment_t                                                         _comment;
		boost::unordered_map<std::string, class_descriptor>               _classes;
		boost::unordered_map<std::string, enum_descriptor>                _enums;
		boost::unordered_map<std::string, friend_descriptor>              _friends;
		boost::unordered_map<std::string, type_alias_descriptor>          _member_types;
		boost::unordered_map<std::string, member_variable_descriptor>     _member_variables;
		constructor_descriptor                                            _constructor;
		std::optional<desctructor_descriptor>                             _destructor;
		boost::unordered_map<std::string, conversion_operator_descriptor> _conversion_operators;
		boost::unordered_map<std::string, member_function_descriptor>     _member_functions;
	};
}

BOOST_FUSION_ADAPT_STRUCT(wtfm::class_descriptor::comment_t, brief, detailed)
