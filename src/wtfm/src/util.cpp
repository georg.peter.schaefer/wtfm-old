#include "util.hpp"

#include <vector>

#include <boost/algorithm/string/join.hpp>
#include <cppast/cpp_entity_kind.hpp>

namespace wtfm {
	auto hierarchy(const cppast::cpp_entity& entity) -> std::vector<const cppast::cpp_entity*> {
		auto parent = entity.parent();
		if (entity.kind() == cppast::cpp_entity_kind::file_t) {
			return {};
		}

		if (!parent.has_value()) {
			return {&entity};
		}
		auto result = hierarchy(parent.value());
		result.emplace_back(&entity);
		return result;
	}

	auto full_qualified_name(const cppast::cpp_entity& entity) -> std::string {
		auto                     hierarchy = wtfm::hierarchy(entity);
		std::vector<std::string> names(hierarchy.size());
		std::transform(hierarchy.begin(), hierarchy.end(), names.begin(), [](auto&& entity) { return entity->name(); });
		return boost::algorithm::join(names, "::");
	}

	auto defining_header(const config& config, const cppast::cpp_entity& entity) -> std::string {
		if (entity.kind() == cppast::cpp_entity_kind::file_t) {
			auto filename = entity.name();
			for (auto include : config.include) {
				include += "/";
				auto prefix_pos = filename.find(include.native());
				if (prefix_pos == 0) {
					auto relative_filename = filename.substr(include.native().size());
					return "<" + relative_filename + ">";
				}
			}
		}
		auto parent = entity.parent();
		if (parent.has_value()) {
			return defining_header(config, parent.value());
		}
		return "";
	}

	auto expression(const cppast::cpp_expression& expression) -> std::string {
		switch (expression.kind()) {
			case cppast::cpp_expression_kind ::literal_t: {
				auto& literal_expression = static_cast<const cppast::cpp_literal_expression&>(expression);
				return literal_expression.value();
			}
			default: {
				auto& unexposed_expression = static_cast<const cppast::cpp_unexposed_expression&>(expression);
				return unexposed_expression.expression().as_string();
			}
		}
	}
}
