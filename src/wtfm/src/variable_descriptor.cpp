#include "variable_descriptor.hpp"

#include "util.hpp"

#include <boost/algorithm/string.hpp>
#include <boost/spirit/home/x3.hpp>

#include <cppast/cpp_storage_class_specifiers.hpp>
#include <cppast/cpp_type.hpp>

#include <mstch/mstch.hpp>

namespace wtfm {
	namespace detail {

		const boost::spirit::x3::rule<struct variable_comment_class, wtfm::variable_descriptor::comment_t>
		        variable_comment     = "variable_comment";
		auto    variable_comment_def = rules::paragraph >> rules::paragraph;
		BOOST_SPIRIT_DEFINE(variable_comment)
	}

	variable_descriptor::variable_descriptor(const wtfm::config& config, const cppast::cpp_variable& entity)
	    : _config(&config), _entity(&entity) {
		entity.comment().map([this](auto&& comment_string) {
			boost::spirit::x3::phrase_parse(comment_string.cbegin() + 1,
			                                comment_string.cend() - 1,
			                                detail::variable_comment,
			                                boost::spirit::x3::space,
			                                _comment);
		});
	}

	auto variable_descriptor::generate() const -> void {
		auto full_qualified_name = wtfm::full_qualified_name(*_entity);
		auto filename            = full_qualified_name + ".html";
		auto path                = _config->output / filename;
		auto parent_name_length  = full_qualified_name.rfind("::");
		auto parent_name =
		        parent_name_length != std::string::npos ? full_qualified_name.substr(0, parent_name_length + 2) : "";
		auto entity_name = parent_name_length != std::string::npos ? full_qualified_name.substr(parent_name_length + 2)
		                                                           : full_qualified_name;

		std::ofstream os(path.native(), std::ofstream::trunc);
		std::ifstream is("template.mustache");
		std::string   page((std::istreambuf_iterator<char>(is)), std::istreambuf_iterator<char>());
		mstch::map    context{{"project_name", _config->name},
                       {"scope", full_qualified_name},
                       {"parent_scope", parent_name},
                       {"name", entity_name},
                       {"defining_header", defining_header()},
                       {"definitions", mstch::array{mstch::map{{"definition", definition()}}}},
                       {"brief", _comment.brief},
                       {"detailed", _comment.detailed}};

		os << mstch::render(page, context) << std::endl;
	}

	auto variable_descriptor::definition() const -> std::string {
		std::string definition;

		if (_entity->is_constexpr()) {
			definition += "constexpr ";
		}

		auto storage_class = _entity->storage_class();
		if (cppast::is_auto(storage_class)) {
			definition += "auto ";
		}
		if (cppast::is_static(storage_class)) {
			definition += "static ";
		}
		if (cppast::is_extern(storage_class)) {
			definition += "extern ";
		}
		if (cppast::is_thread_local(storage_class)) {
			definition += "thread_local ";
		}

		auto type = cppast::to_string(_entity->type());
		if (auto range = boost::find_first(type, " const"); range) {
			boost::erase_all(type, range);
			type = "const " + type;
		}

		definition += type + " " + _entity->name();

		auto default_value = _entity->default_value();
		if (default_value.has_value()) {
			definition += " = " + wtfm::expression(default_value.value());
		}

		definition += ";";

		return definition;
	}
}
