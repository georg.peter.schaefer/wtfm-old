#include "html.hpp"

namespace wtfm {
	html_page::definition_table::definition_table(std::ostream& os, const std::string& defining_header) : _os(os) {
		_os << "<table class=\"member-table-begin\">" << std::endl;
		_os << "<tbody>" << std::endl;
		_os << "<tr class=\"member-table-header\">" << std::endl;
		_os << "<td>" << std::endl;
		_os << "<div>"
		    << "Defined in header "
		    << "<code>" << defining_header << "</code>"
		    << "</div>" << std::endl;
		_os << "</td>" << std::endl;
		_os << "<td>" << std::endl;
		_os << "</td>" << std::endl;
		_os << "</tr>" << std::endl;
	}

	html_page::definition_table::~definition_table() {
		_os << "</tbody>" << std::endl;
		_os << "</table>" << std::endl;
	}

	auto html_page::definition_table::add_entry(const std::string& definition, const std::string& since)
	        -> definition_table& {
		_os << "<tr class=\"member-table-entry\">" << std::endl;
		_os << "<td>" << std::endl;
		_os << "<div class=\"member-table-entry-div\">" << std::endl;
		_os << "<div>" << std::endl;
		_os << "<span>" << std::endl;
		_os << definition << std::endl;
		_os << "</span>" << std::endl;
		_os << "</div>" << std::endl;
		_os << "</div>" << std::endl;
		_os << "</td>" << std::endl;
		_os << "<td>" << std::endl;
		_os << since << std::endl;
		_os << "</td>" << std::endl;
		_os << "</tr>" << std::endl;

		return *this;
	}

	html_page::member_table::member_table(std::ostream& os, const std::string& name) : _os(os) {
		_os << "<h3 class=\"member-table-heading\">" << std::endl;
		_os << "<span>" << name << "</span>" << std::endl;
		_os << "</h3>" << std::endl;
		_os << "<table class=\"member-table-begin\">" << std::endl;
		_os << "<tbody>" << std::endl;
	}

	html_page::member_table::~member_table() {
		_os << "</tbody>" << std::endl;
		_os << "</table>" << std::endl;
	}

	auto html_page::member_table::add_entry(const std::string& name, const std::string& brief) -> member_table& {
		_os << "<tr class=\"member-table-entry\">" << std::endl;
		_os << "<td>" << std::endl;
		_os << "<div class=\"member-table-entry-div\">" << std::endl;
		_os << "<div>" << std::endl;
		_os << "<span>" << std::endl;
		_os << name << std::endl;
		_os << "</span>" << std::endl;
		_os << "</div>" << std::endl;
		_os << "</div>" << std::endl;
		_os << "</td>" << std::endl;
		_os << "<td>" << std::endl;
		_os << brief << std::endl;
		_os << "</td>" << std::endl;
		_os << "</tr>" << std::endl;

		return *this;
	}

	html_page::html_page(const config& config, std::ostream& os, const std::string& name) : _config(&config), _os(os) {
		_os << "<!doctype html>" << std::endl;
		_os << "<html>" << std::endl;
		_os << "<head>" << std::endl;
		_os << "<title>" << name << " - " << _config->name << "</title>" << std::endl;
		_os << "<meta charset=\"UTF-8\">" << std::endl;
		_os << "<link rel=stylesheet href=styles.css>" << std::endl;
		_os << "<link rel=stylesheet href=highlight/styles/default.css>" << std::endl;
		_os << "<script src=\"highlight/highlight.pack.js\"></script>" << std::endl;
		_os << "<script>hljs.initHighlightingOnLoad();</script>" << std::endl;
		_os << "</head>" << std::endl;
		_os << "<body>" << std::endl;
		_os << "<div id=\"main-head\">" << std::endl;
		_os << "<div id=\"head-first-base\">" << std::endl;
		_os << "<div id=\"head-first\">" << std::endl;
		_os << "<h5>" << config.name << "</h5>" << std::endl;
		_os << "</div>" << std::endl;
		_os << "</div>" << std::endl;
		_os << "<div id=\"head-second-base\">" << std::endl;
		_os << "<div id=\"head-second\">" << std::endl;
		_os << "</div>" << std::endl;
		_os << "</div>" << std::endl;
		_os << "</div>" << std::endl;
		_os << "<div id=\"content-base\">" << std::endl;
		_os << "<div id=\"content\">" << std::endl;
	}

	html_page::~html_page() {
		_os << "</div>" << std::endl;
		_os << "</div>" << std::endl;
		_os << "</body>" << std::endl;
		_os << "</html>" << std::endl;
	}

	auto html_page::add_heading(const std::string& name) -> html_page& {
		auto parent_name_length = name.rfind("::");
		auto parent_name        = parent_name_length != std::string::npos ? name.substr(0, parent_name_length + 2) : "";
		auto entity_name        = parent_name_length != std::string::npos ? name.substr(parent_name_length + 2) : name;
		_os << "<h1 id=\"first-heading\">"
		    << "<span style=\"font-size:0.7em; font-weight: unset; line-height:130%\">" << parent_name << "</span>"
		    << entity_name << "</h1>" << std::endl;

		return *this;
	}
}