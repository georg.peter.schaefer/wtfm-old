#include "enum_value_descriptor.hpp"

#include "util.hpp"

#include <boost/spirit/home/x3.hpp>

namespace wtfm {
	namespace detail {
		const boost::spirit::x3::rule<struct enum_value_comment_class, wtfm::enum_value_descriptor::comment_t>
		        enum_value_comment     = "enum_value_comment";
		auto    enum_value_comment_def = rules::paragraph;
		BOOST_SPIRIT_DEFINE(enum_value_comment)
	}

	enum_value_descriptor::enum_value_descriptor(const config& config, const cppast::cpp_enum_value& entity)
	    : _config(&config), _entity(&entity) {
		entity.comment().map([this](auto&& comment_string) {
			boost::spirit::x3::phrase_parse(comment_string.cbegin() + 1,
			                                comment_string.cend() - 1,
			                                detail::enum_value_comment,
			                                boost::spirit::x3::space,
			                                _comment);
		});
	}
}
