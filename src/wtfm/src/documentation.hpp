#pragma once

#include "config.hpp"
#include "namespace_descriptor.hpp"
#include "util.hpp"

#include <memory>
#include <stack>
#include <string>
#include <variant>
#include <vector>


#include <boost/unordered_map.hpp>

#include <cppast/cpp_friend.hpp>
#include <cppast/cpp_member_function.hpp>
#include <cppast/cpp_variable.hpp>
#include <cppast/libclang_parser.hpp>

namespace wtfm {
	class documentation {
	public:
		documentation(const config& config);

		auto gather() -> void;
		auto generate() -> void;

	private:
		using container_node = std::variant<namespace_descriptor*, class_descriptor*, enum_descriptor*>;

		auto _parse_files(const std::vector<boost::filesystem::path>& files)
		        -> std::vector<std::unique_ptr<cppast::cpp_file>>;


		auto _handle_container(namespace_descriptor* container, const cppast::cpp_namespace& entity) -> void {
			auto iterator = container->add(entity);
			_container_hierarchy.push(&iterator->second);
		}

		auto _handle_container(namespace_descriptor* container, const cppast::cpp_class& entity) -> void {
			auto iterator = container->add(entity);
			_container_hierarchy.push(&iterator->second);
		}

		auto _handle_container(namespace_descriptor* container, const cppast::cpp_function& entity) -> void {
			container->add(entity);
		}

		auto _handle_container(namespace_descriptor* container, const cppast::cpp_enum& entity) -> void {
			auto iterator = container->add(entity);
			_container_hierarchy.push(&iterator->second);
		}

		auto _handle_container(namespace_descriptor* container, const cppast::cpp_variable& entity) -> void {
			container->add(entity);
		}

		auto _handle_container(class_descriptor* container, const cppast::cpp_class& entity) -> void {
			auto iterator = container->add(entity);
			_container_hierarchy.push(&iterator->second);
		}

		auto _handle_container(class_descriptor* container, const cppast::cpp_enum& entity) -> void {
			auto iterator = container->add(entity);
			_container_hierarchy.push(&iterator->second);
		}

		auto _handle_container(class_descriptor* container, const cppast::cpp_friend& entity) -> void {
			container->add(entity);
		}

		auto _handle_container(class_descriptor* container, const cppast::cpp_type_alias& entity) -> void {
			container->add(entity);
		}

		auto _handle_container(class_descriptor* container, const cppast::cpp_member_variable& entity) -> void {
			container->add(entity);
		}

		auto _handle_container(class_descriptor* container, const cppast::cpp_constructor& entity) -> void {
			container->add(entity);
		}

		auto _handle_container(class_descriptor* container, const cppast::cpp_destructor& entity) -> void {
			container->destructor(entity);
		}

		auto _handle_container(class_descriptor* container, const cppast::cpp_conversion_op& entity) -> void {
			container->add(entity);
		}

		auto _handle_container(class_descriptor* container, const cppast::cpp_member_function& entity) -> void {
			container->add(entity);
		}

		auto _handle_container(enum_descriptor* container, const cppast::cpp_enum_value& entity) -> void {
			container->add(entity);
		}

		template <typename Container, typename Entity>
		auto _handle_container(Container*, const Entity&) -> void {}

		template <typename Entity>
		inline auto _visit_container(const Entity& entity) {
			auto& node = _container_hierarchy.top();
			std::visit([this, &entity](auto container) { this->_handle_container(container, entity); }, node);
		}

		template <typename Entity>
		inline auto _handle_entity(const Entity& entity) -> void {
			_visit_container(entity);
		}

		config                                                  _config;
		cppast::cpp_entity_index                                _index;
		cppast::libclang_parser                                 _parser;
		cppast::libclang_compile_config                         _default_compilation_config;
		std::vector<std::unique_ptr<cppast::cpp_file>>          _files;
		std::stack<container_node>                              _container_hierarchy;
		boost::unordered_map<std::string, namespace_descriptor> _namespaces;
	};
}
