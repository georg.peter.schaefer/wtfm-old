#pragma once

#include "config.hpp"
#include "enum_value_descriptor.hpp"
#include "util.hpp"

#include <string>
#include <unordered_map>

#include <boost/fusion/include/adapt_struct.hpp>

#include <cppast/cpp_enum.hpp>

namespace wtfm {
	class enum_descriptor {
	public:
		struct comment_t {
			std::string brief;
			std::string detailed;
		};

		enum_descriptor(const config& config, const cppast::cpp_enum& entity);

		auto add(const cppast::cpp_enum_value& entity) {
			auto [iterator, emplaced] = _values.emplace(entity.name(), enum_value_descriptor(*_config, entity));
			return iterator;
		}

		auto generate() const -> void;

		auto full_qualified_name() const {
			return wtfm::full_qualified_name(*_entity);
		}

		auto defining_header() const {
			return wtfm::defining_header(*_config, *_entity);
		}

		auto definition() const -> std::string;

		auto& comment() const {
			return _comment;
		}

	private:
		const config*                                          _config;
		const cppast::cpp_enum*                                _entity;
		comment_t                                              _comment;
		std::unordered_map<std::string, enum_value_descriptor> _values;
	};
}

BOOST_FUSION_ADAPT_STRUCT(wtfm::enum_descriptor::comment_t, brief, detailed)
