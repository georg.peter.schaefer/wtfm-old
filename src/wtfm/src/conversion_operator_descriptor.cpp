#include "conversion_operator_descriptor.hpp"

#include "link_resolver.hpp"

#include <iostream>

#include <boost/spirit/home/x3.hpp>

#include <mstch/mstch.hpp>


namespace wtfm {
	namespace detail {
		const boost::spirit::x3::rule<struct conversion_operator_comment_class,
		                              wtfm::conversion_operator_descriptor::comment_t>
		        conversion_operator_comment     = "conversion_operator_comment";
		auto    conversion_operator_comment_def = rules::paragraph >> rules::paragraph;
		BOOST_SPIRIT_DEFINE(conversion_operator_comment)
	}


	conversion_operator_descriptor::conversion_operator_descriptor(const config&                    config,
	                                                               const cppast::cpp_conversion_op& entity)
	    : _config(&config), _entity(&entity) {
		entity.comment().map([this](auto&& comment_string) {
			boost::spirit::x3::phrase_parse(comment_string.cbegin() + 1,
			                                comment_string.cend() - 1,
			                                detail::conversion_operator_comment,
			                                boost::spirit::x3::space,
			                                _comment);
		});
	}

	auto conversion_operator_descriptor::generate() const -> void {
		auto full_qualified_name = conversion_operator_descriptor::full_qualified_name();
		auto filename            = full_qualified_name + ".html";
		auto path                = _config->output / filename;
		auto parent_name_length  = full_qualified_name.rfind("::");
		auto parent_name =
		        parent_name_length != std::string::npos ? full_qualified_name.substr(0, parent_name_length + 2) : "";
		auto entity_name = parent_name_length != std::string::npos ? full_qualified_name.substr(parent_name_length + 2)
		                                                           : full_qualified_name;


		std::ofstream os(path.native(), std::ofstream::trunc);
		std::ifstream is("conversion_operator.mustache");
		std::string   page((std::istreambuf_iterator<char>(is)), std::istreambuf_iterator<char>());
		mstch::map    context{{"project_name", _config->name},
                       {"scope", full_qualified_name},
                       {"parent_scope", parent_name},
                       {"name", entity_name},
                       {"brief", _comment.brief},
                       {"detailed", _comment.detailed},
                       {"noexcept", noexcept_specifier()},
                       {"body_type", body_type()}};

		os << mstch::render(page, context) << std::endl;
	}
}
