#pragma once

#include "config.hpp"
#include "util.hpp"

#include <string>

#include <boost/fusion/include/adapt_struct.hpp>

#include <cppast/cpp_enum.hpp>

namespace wtfm {
	class enum_value_descriptor {
	public:
		struct comment_t {
			std::string detailed;
		};

		enum_value_descriptor(const config& config, const cppast::cpp_enum_value& entity);

		auto name() {
			return _entity->name();
		}

		auto& comment() const {
			return _comment;
		}

		auto value() const {
			if (auto maybe = _entity->value(); maybe.has_value()) {
				auto& value = maybe.value();
				return expression(value);
			}

			return std::string("/* unspecified */");
		}

	private:
		const config*                 _config [[maybe_unused]];
		const cppast::cpp_enum_value* _entity;
		comment_t                     _comment;
	};
}

BOOST_FUSION_ADAPT_STRUCT(wtfm::enum_value_descriptor::comment_t, detailed)
