#pragma once

#include "config.hpp"
#include "util.hpp"

#include <string>

#include <boost/fusion/include/adapt_struct.hpp>

#include <cppast/cpp_member_function.hpp>

namespace wtfm {
	class conversion_operator_descriptor {
	public:
		struct comment_t {
			std::string brief;
			std::string detailed;
		};

		conversion_operator_descriptor(const config& config, const cppast::cpp_conversion_op& entity);

		auto generate() const -> void;

		auto full_qualified_name() const {
			return wtfm::full_qualified_name(*_entity);
		}

		auto noexcept_specifier() const -> std::string {
			return _entity->noexcept_condition().has_value() ? " noexcept" : std::string();
		}

		auto body_type() const -> std::string {
			switch (_entity->body_kind()) {
				case cppast::cpp_function_body_kind::cpp_function_declaration:
				case cppast::cpp_function_body_kind::cpp_function_definition:
					return ";";
				case cppast::cpp_function_body_kind ::cpp_function_defaulted:
					return " = default;";
				case cppast::cpp_function_body_kind::cpp_function_deleted:
					return " = delete;";
				default:
					throw std::runtime_error("Unhandled function body type.");
			}
		}

		auto& comment() const {
			return _comment;
		}

	private:
		const config*                    _config;
		const cppast::cpp_conversion_op* _entity;
		comment_t                        _comment;
	};
}

BOOST_FUSION_ADAPT_STRUCT(wtfm::conversion_operator_descriptor::comment_t, brief, detailed)
