#pragma once

#include "class_descriptor.hpp"
#include "enum_descriptor.hpp"
#include "function_descriptor.hpp"
#include "util.hpp"
#include "variable_descriptor.hpp"

#include <iostream>
#include <string>

#include <boost/unordered_map.hpp>

#include <cppast/cpp_namespace.hpp>

namespace wtfm {
	struct config;

	class namespace_descriptor {
	public:
		struct comment_t {
			std::string brief;
			std::string detailed;
		};

		namespace_descriptor(const config& config);

		namespace_descriptor(const config& config, const cppast::cpp_namespace& entity);

		auto add(const cppast::cpp_namespace& entity) {
			auto [iterator, emplaced] = _namespaces.emplace(entity.name(), namespace_descriptor(*_config, entity));
			return iterator;
		}

		auto add(const cppast::cpp_class& entity) {
			auto [iterator, emplaced] = _classes.emplace(entity.name(), class_descriptor(*_config, entity));
			return iterator;
		}

		auto add(const cppast::cpp_function& entity) {
			if (auto function_it = _functions.find(entity.name()); function_it != _functions.end()) {
				function_it->second.add(entity);
				return function_it;
			} else {
				auto [iterator, emplaced] = _functions.emplace(entity.name(), function_descriptor(*_config));
				iterator->second.add(entity);
				return iterator;
			}
		}

		auto add(const cppast::cpp_enum& entity) {
			auto [iterator, emplaced] = _enums.emplace(entity.name(), enum_descriptor(*_config, entity));
			return iterator;
		}

		auto add(const cppast::cpp_variable& entity) {
			auto [iterator, emplaced] = _variables.emplace(entity.name(), variable_descriptor(*_config, entity));
			return iterator;
		}

		auto generate() const -> void;

		auto full_qualified_name() const {
			if (_entity) {
				return wtfm::full_qualified_name(*_entity);
			}
			return std::string();
		}

		auto definition() const {
			if (_entity) {
				return "namspace " + _entity->name() + ";";
			}
			return std::string();
		}

		auto& comment() const {
			return _comment;
		}

	private:
		const config*                _config;
		const cppast::cpp_namespace* _entity;

		comment_t                                               _comment;
		boost::unordered_map<std::string, namespace_descriptor> _namespaces;
		boost::unordered_map<std::string, class_descriptor>     _classes;
		boost::unordered_map<std::string, function_descriptor>  _functions;
		boost::unordered_map<std::string, enum_descriptor>      _enums;
		boost::unordered_map<std::string, variable_descriptor>  _variables;
	};
}
