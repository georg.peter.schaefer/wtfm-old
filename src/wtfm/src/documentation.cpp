#include "documentation.hpp"

#include <algorithm>
#include <iostream>
#include <numeric>
#include <thread>

#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/range/irange.hpp>

#include <cppast/code_generator.hpp>
#include <cppast/visitor.hpp>

namespace wtfm {
	documentation::documentation(const wtfm::config& config) : _config(config) {
		if (boost::filesystem::exists(config.output)) {
			boost::filesystem::remove_all(config.output);
		}

		boost::filesystem::create_directory(config.output);

		_default_compilation_config.set_flags(cppast::cpp_standard::cpp_2a);
		_default_compilation_config.fast_preprocessing(false);
		for (auto&& include : config.include) {
			_default_compilation_config.add_include_dir(include.native());
		}
		if (_config.features.has_value()) {
			for (auto& feature : *_config.features) {
				_default_compilation_config.enable_feature(feature);
			}
		}

		auto [iterator, emplaced] = _namespaces.emplace("", namespace_descriptor(_config));
		_container_hierarchy.push(&iterator->second);
	}

	auto documentation::gather() -> void {
		std::vector<boost::filesystem::path> input_files;

		std::vector<boost::filesystem::path> exclude_directories;
		if (_config.exclude) {
			exclude_directories = *_config.exclude;
		}

		for (auto&& input : _config.input) {
			auto directory_iterator = boost::filesystem::recursive_directory_iterator(input);
			for (auto&& entry : directory_iterator) {
				auto&& path = entry.path();
				if (auto exclude_it = std::find(exclude_directories.begin(), exclude_directories.end(), path);
				    exclude_it != exclude_directories.end()) {
					directory_iterator.disable_recursion_pending(true);
				}
				if (boost::filesystem::is_regular_file(path) && (path.extension() == ".hpp")) {
					input_files.emplace_back(path);
				}
			}
		}

		_files = _parse_files(input_files);

		for (auto&& file : _files) {
			if (!file) {
				std::cerr << "File is empty!" << std::endl;
				continue;
			}

			cppast::visit(*file, [&](const cppast::cpp_entity& e, const cppast::visitor_info& info) {
				if (info.is_old_entity()) {
					switch (e.kind()) {
						case cppast::cpp_entity_kind ::namespace_t:
						case cppast::cpp_entity_kind ::class_t:
						case cppast::cpp_entity_kind ::enum_t:
							_container_hierarchy.pop();
							return;
						default:
							return;
					}
				}

				switch (e.kind()) {
					case cppast::cpp_entity_kind::namespace_t:
						_handle_entity(static_cast<const cppast::cpp_namespace&>(e));
						break;
					case cppast::cpp_entity_kind::class_t:
						_handle_entity(static_cast<const cppast::cpp_class&>(e));
						break;
					case cppast::cpp_entity_kind::friend_t:
						_handle_entity(static_cast<const cppast::cpp_friend&>(e));
						break;
					case cppast::cpp_entity_kind::type_alias_t:
						_handle_entity(static_cast<const cppast::cpp_type_alias&>(e));
						break;
					case cppast::cpp_entity_kind::member_variable_t:
						_handle_entity(static_cast<const cppast::cpp_member_variable&>(e));
						break;
					case cppast::cpp_entity_kind::constructor_t:
						_handle_entity(static_cast<const cppast::cpp_constructor&>(e));
						break;
					case cppast::cpp_entity_kind::destructor_t:
						_handle_entity(static_cast<const cppast::cpp_destructor&>(e));
						break;
					case cppast::cpp_entity_kind::conversion_op_t:
						_handle_entity(static_cast<const cppast::cpp_conversion_op&>(e));
						break;
					case cppast::cpp_entity_kind::member_function_t:
						_handle_entity(static_cast<const cppast::cpp_member_function&>(e));
						break;
					case cppast::cpp_entity_kind ::function_t:
						_handle_entity(static_cast<const cppast::cpp_function&>(e));
						break;
					case cppast::cpp_entity_kind::enum_t:
						_handle_entity(static_cast<const cppast::cpp_enum&>(e));
						break;
					case cppast::cpp_entity_kind ::enum_value_t:
						_handle_entity(static_cast<const cppast::cpp_enum_value&>(e));
						break;
					case cppast::cpp_entity_kind ::variable_t:
						_handle_entity(static_cast<const cppast::cpp_variable&>(e));
						break;
					default:
						std::cout << "unhandled type: " << cppast::to_string(e.kind()) << std::endl;
						break;
				}
			});
		}
	}

	auto documentation::generate() -> void {
		if (boost::filesystem::exists(_config.output / "styles.css")) {
			boost::filesystem::remove_all(_config.output / "styles.css");
		}
		boost::filesystem::copy_file("styles.css", _config.output / "styles.css");

		for (auto [key, namespace_descriptor] : _namespaces) {
			namespace_descriptor.generate();
		}
	}

	auto documentation::_parse_files(const std::vector<boost::filesystem::path>& files)
	        -> std::vector<std::unique_ptr<cppast::cpp_file>> {
		auto database = !_config.compile_commands.has_value()
		                        ? type_safe::optional<cppast::libclang_compilation_database>()
		                        : cppast::libclang_compilation_database(_config.compile_commands->native());

		using result_type = std::vector<std::unique_ptr<cppast::cpp_file>>;

		auto map = [&](auto& file) -> std::unique_ptr<cppast::cpp_file> {
			try {
				std::clog << "Parsing file" << file << std::endl;
				auto config = database.map([&](const cppast::libclang_compilation_database& db) {
					return cppast::find_config_for(db, file.native());
				});

				config.map([config = _config](cppast::libclang_compile_config& compile_config) {
					for (auto include_directory : config.include) {
						compile_config.set_flags(cppast::cpp_standard::cpp_latest);
						compile_config.add_include_dir(include_directory.native());
					}
					if (config.features.has_value()) {
						for (auto& feature : *config.features) {
							compile_config.enable_feature(feature);
						}
					}
				});

				return _parser.parse(_index, file.native(), config.value_or(_default_compilation_config));
			} catch (const std::runtime_error& ex) {
				std::cerr << ex.what() << std::endl;
				_parser.reset_error();
				return {};
			}
		};

		std::vector<std::vector<boost::filesystem::path>> ranges;
		auto                                              pool_size = std::max(1u, std::thread::hardware_concurrency());
		auto files_per_thread                                       = std::max(std::size_t(1), files.size() / pool_size);
		auto current_range_size                                     = files_per_thread;
		for (auto&& file : files) {
			if (current_range_size == files_per_thread) {
				current_range_size = 0;
				ranges.emplace_back();
			}
			ranges.back().emplace_back(file);
			++current_range_size;
		}

		result_type result;
		result.reserve(files.size());
		std::vector<std::thread> pool;
		std::mutex               result_lock;
		for (auto&& range : ranges) {
			pool.emplace_back([&range, &map, &result, &result_lock]() {
				result_type partial_result;
				partial_result.reserve(range.size());
				std::transform(range.begin(), range.end(), std::back_inserter(partial_result), map);
				std::lock_guard guard(result_lock);
				std::move(partial_result.begin(), partial_result.end(), std::back_inserter(result));
			});
		}

		for (auto&& thread : pool) {
			thread.join();
		}

		return result;
	}
}
