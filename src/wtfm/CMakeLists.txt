configure_file(src/info.hpp.in ${CMAKE_CURRENT_BINARY_DIR}/info.hpp)

add_executable(wtfm
		src/main.cpp
		src/documentation.cpp
		src/namespace_descriptor.cpp
		src/class_descriptor.cpp
		src/member_variable_descriptor.cpp
		src/constructor_descriptor.cpp
		src/destructor_descriptor.cpp
		src/conversion_operator_descriptor.cpp
		src/member_function_descriptor.cpp
		src/function_descriptor.cpp
		src/enum_descriptor.cpp
		src/enum_value_descriptor.cpp
		src/variable_descriptor.cpp
		src/html.cpp
		src/link_resolver.cpp
		src/util.cpp)
target_include_directories(wtfm
		PRIVATE
		$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/src>
		${CMAKE_CURRENT_BINARY_DIR})
target_link_libraries(wtfm
		PRIVATE
		Boost::boost
		Boost::filesystem
		Boost::program_options
		cppast
		mstch)
target_compile_features(wtfm
		PRIVATE
		cxx_std_20)
target_compile_options(wtfm
		PRIVATE
		-Wall
		-Wextra
		-Wpedantic
		-Werror
		$<$<CXX_COMPILER_ID:Clang>:-stdlib=libc++>
		$<$<CXX_COMPILER_ID:GNU>:-Wno-error=attributes>)
target_link_options(wtfm
		PRIVATE
		$<$<CXX_COMPILER_ID:Clang>:-stdlib=libc++>)
install(TARGETS wtfm EXPORT wtfm
		RUNTIME DESTINATION bin)
add_dependencies(wtfm styles template function class member_variable constructor destructor conversion_operator member_function)

add_custom_target(styles
		COMMAND sassc ${CMAKE_CURRENT_SOURCE_DIR}/src/styles.scss ${CMAKE_CURRENT_BINARY_DIR}/styles.css
		COMMENT "Compiling styles.scss")
add_custom_target(template
		COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/src/template.mustache ${CMAKE_CURRENT_BINARY_DIR}/template.mustache
		COMMENT "Copying template.mustache")
add_custom_target(function
		COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/src/function.mustache ${CMAKE_CURRENT_BINARY_DIR}/function.mustache
		COMMENT "Copying function.mustache")
add_custom_target(class
		COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/src/class.mustache ${CMAKE_CURRENT_BINARY_DIR}/class.mustache
		COMMENT "Copying class.mustache")
add_custom_target(member_variable
		COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/src/member_variable.mustache ${CMAKE_CURRENT_BINARY_DIR}/member_variable.mustache
		COMMENT "Copying member_variable.mustache")
add_custom_target(constructor
		COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/src/constructor.mustache ${CMAKE_CURRENT_BINARY_DIR}/constructor.mustache
		COMMENT "Copying constructor.mustache")
add_custom_target(destructor
		COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/src/destructor.mustache ${CMAKE_CURRENT_BINARY_DIR}/destructor.mustache
		COMMENT "Copying destructor.mustache")
add_custom_target(conversion_operator
		COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/src/conversion_operator.mustache ${CMAKE_CURRENT_BINARY_DIR}/conversion_operator.mustache
		COMMENT "Copying conversion_operator.mustache")
add_custom_target(member_function
		COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/src/member_function.mustache ${CMAKE_CURRENT_BINARY_DIR}/member_function.mustache
		COMMENT "Copying member_function.mustache")
